﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    // Obracanie kamery

    void Update()
    {
        if (GameObject.Find("GameManager").GetComponent<GameManager>().startGame)
        {
            if (Input.GetKeyDown(KeyCode.A)) gameObject.transform.Rotate(0, gameObject.transform.rotation.y - 90f, 0);
            if (Input.GetKeyDown(KeyCode.D)) gameObject.transform.Rotate(0, gameObject.transform.rotation.y + 90, 0);
        }
    }
}
