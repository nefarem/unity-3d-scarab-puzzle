﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{

    // Tworzenie linii między dwoma skarabeuszami.

    public GameObject start;
    public GameObject target;

    [System.Obsolete]
    public Line(GameObject start, GameObject target, Material material)
    {
        this.start = start;
        this.target = target;

        GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        GameObject line = new GameObject("Line");

        line.transform.position = start.transform.position;
        line.AddComponent<LineRenderer>();
        line.AddComponent<Line>();

        LineRenderer lineRenderer = line.GetComponent<LineRenderer>();
        Line lineComponent = line.GetComponent<Line>();

        lineRenderer.SetPosition(0, new Vector3(start.transform.position.x, start.transform.position.y, start.transform.position.z));
        lineRenderer.SetPosition(1, new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z));
        lineRenderer.startWidth = 0.28f;
        lineRenderer.endWidth = 0.28f;
        lineRenderer.material = material;
        lineRenderer.SetColors(Color.cyan, Color.cyan);
        lineRenderer.sortingOrder = 1;

        lineComponent.start = this.start;
        lineComponent.target = this.target;

        start.GetComponent<Scarab>().createdConnects.Add(line);
        target.GetComponent<Scarab>().createdConnects.Add(line);

        gameManager.actualConnects++;

    }
}
