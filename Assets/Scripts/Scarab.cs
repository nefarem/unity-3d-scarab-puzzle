﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scarab : MonoBehaviour
{
    public GameObject[] neighbourScarabs; // Sąsiednie skarabeusze do obecnego.
    public List<GameObject> createdConnects; // Utworzone połączenia do tego skarabeusza

    public Sprite[] spritesScarab;        // Sprite'y skarabeusza (stone, gold, blue).

    [HideInInspector]
    public GameManager gameManager;

    public int maxConnects;        // Maksymalna liczba połączeń do tego skarabeusza.

    public Material material;    // Materiał linii

    Color startColor;
    RaycastHit hit;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10;

        Vector3 screenPos = Camera.main.ScreenToWorldPoint(mousePos);

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
        }
    }

    // Podświetlenie skarabeusza gdy zostanie najechany myszką.
    void OnMouseEnter()
    {
        startColor = GetComponent<Renderer>().material.color;
        GetComponent<Renderer>().material.color = Color.yellow;
    }

    // Wyłączenie podświetlenia.
    void OnMouseExit()
    {
        GetComponent<Renderer>().material.color = startColor;
    }

    [System.Obsolete]
    void OnMouseDown()
    {
        // Wybranie początkowego skarabeusza

        if (gameManager.startGame && hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite == hit.collider.gameObject.GetComponent<Scarab>().spritesScarab[0])
        {
            gameManager.startGame = false;
            gameManager.HideInfromation();
            gameManager.prevoiusScarab = hit.collider.gameObject;

            gameObject.GetComponent<SpriteRenderer>().sprite = spritesScarab[1];

            gameManager.playingBoard = gameObject.transform.parent;
            gameManager.playingBoard.gameObject.layer = 2;

        }

        // Wybranie następnego skarabeusza + sprawdzenie:
        // 1. Czy wybrany następny skarabeusz jest sąsiadujący do obecnego.
        // 2. Czy nie powieli już wcześniej utworzonej ścieżki. 
        // 3. Czy utworzona ściezka nie spowoduje że gracz nie będzie miał możliwości dalszego ruchu.

        else
        {
            if (IsNeighbour() && !CheckDuplicateConnect())
            {
                gameManager.prevoiusScarab.GetComponent<SpriteRenderer>().sprite = spritesScarab[2];

                Line line = new Line(gameManager.prevoiusScarab, hit.collider.gameObject, material);

                gameManager.prevoiusScarab = hit.collider.gameObject;
                gameManager.prevoiusScarab.GetComponent<SpriteRenderer>().sprite = spritesScarab[1];
            }

            // Wyświetlenie panelu z porażką w momencie gdy gracz nie będzie miał więcej ruchów a zagadka nie została rozwiązana.
            if (gameManager.maxConnects != gameManager.actualConnects && gameManager.prevoiusScarab.GetComponent<Scarab>().createdConnects.Count == gameManager.prevoiusScarab.GetComponent<Scarab>().maxConnects)
                gameManager.ShowLosePanel();
        }
    }

    // Metoda sprawdzająca czy mająca powstać ścieżka nie będzie duplikatem już istniejącej.

    // false - Nie spowoduje zduplikowania istniejącej ścieżki.
    // true - Spowoduje zdublowanie istniejącej ścieżki.

    bool CheckDuplicateConnect()
    {
        if (hit.collider.gameObject.GetComponent<Scarab>().createdConnects.Count > 0)
        {
            foreach (GameObject item in gameManager.prevoiusScarab.GetComponent<Scarab>().createdConnects)
            {

                if (item.GetComponent<Line>().start.name == hit.collider.gameObject.name || item.GetComponent<Line>().target.name == hit.collider.gameObject.name)
                    return true;

            }
        }
        else return false;

        return false;
    }

    // Metoda sprawdzająca czy wybrany następny skarabeusz jest sąsiadujący do obecnego.

    // false - Wybrany następny skarabeusz nie sąsiaduje z obecnym.
    // true - Wybrany skarabeusz sąsiaduje z obecnym.

    bool IsNeighbour()
    {
        foreach (GameObject item in gameManager.prevoiusScarab.GetComponent<Scarab>().neighbourScarabs)
        {
            if (item.gameObject.name == hit.collider.gameObject.name) return true;
        }
        return false;
    }

}
