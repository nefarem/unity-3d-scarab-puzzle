﻿using UnityEngine;

public class Interface : MonoBehaviour
{

    // Jeżeli gracz zdecyduje się dalej kontynuować rozgrywkę po ukończeniu jednej z plansz.
    public void WinGame()
    {
        GameObject.Find("GameManager").GetComponent<GameManager>().startGame = true;
        GameObject.Find("GameManager").GetComponent<GameManager>().informationPanel.SetActive(true);
        GameObject.Find("GameManager").GetComponent<GameManager>().winPanel.SetActive(false);
    }

    // Jeżeli gracz zdecyduje się zrestartować rozgrywkę po przegranej.
    public void RestartGame()
    {
        foreach (Transform child in GameObject.Find("GameManager").GetComponent<GameManager>().playingBoard)
        {
            if (child.tag == "Scarab" && child.gameObject.GetComponent<Scarab>().createdConnects.Count > 0)
            {
                child.gameObject.GetComponent<SpriteRenderer>().sprite = child.gameObject.GetComponent<Scarab>().spritesScarab[0];

                foreach (GameObject item in child.gameObject.GetComponent<Scarab>().createdConnects) Destroy(item);

                child.gameObject.GetComponent<Scarab>().createdConnects.Clear();

                GameObject.Find("GameManager").GetComponent<GameManager>().actualConnects = 0;
                GameObject.Find("GameManager").GetComponent<GameManager>().losePanel.SetActive(false);
                GameObject.Find("GameManager").GetComponent<GameManager>().startGame = true;
                GameObject.Find("GameManager").GetComponent<GameManager>().informationPanel.SetActive(true);


            }
        }
    }

    // Wyjście z gry.
    public void ExitGame()
    {
        Application.Quit();
    }
}
