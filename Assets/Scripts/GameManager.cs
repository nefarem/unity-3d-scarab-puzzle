﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public GameObject prevoiusScarab;     // Obecny skarabeusz z którego gracz wykonuje ruch do następnego.
    public GameObject informationPanel;   // Panel UI z podstawowymi informacjami.
    public GameObject winPanel;           // Panel UI z wygraną.
    public GameObject losePanel;          // Panel UI z przegraną.

    [HideInInspector]
    public Transform playingBoard;      // Aktualnie rozgrywana plansza.

    public int maxConnects;             // Maksymalna liczba połączeń na jednej planszy.
    public int actualConnects;          // Aktualnie utworzone połączenia na rozgrywanej planszy.
    public int compleatedBoards;        // Liczba ukończonych plansz.

    public bool startGame = true;      // Sygnał do rozpoczęcia nowej rozgrywki (po porażce lub wygranej)

    void Start()
    {
        maxConnects = 42;
        actualConnects = 0;
        compleatedBoards = 0;
    }

    void Update()
    {
        // Jeżeli zostaną utworzone pomyślnie wszystkie połączenia na danej planszy, pojawi się okno z wygraną.
        if (maxConnects == actualConnects)
        {
            actualConnects = 0;
            ShowWinPanel();
        }

        // Jeżeli zostaną ukończone wszystkie 4 plansze gra wyłączy się.
        if (compleatedBoards == 4) Application.Quit();
        
    }

    // Ukrycie panelu z informacją.
    public void HideInfromation()
    {
        informationPanel.SetActive(false);
    }

    // Wyświetlenie panelu z przegraną.
    public void ShowLosePanel()
    {
        losePanel.SetActive(true);
    }

    // Wyświetlenie panelu z wygraną.
    public void ShowWinPanel()
    {
        compleatedBoards++;
        winPanel.SetActive(true);
    }
}
